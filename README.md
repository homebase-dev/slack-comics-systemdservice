# slack-comics-systemdservice

Send comicstrips to your slack channel using some bash scripts which fetch the comic-image-url from the website and send them via slack web api. The scripts are called via a systemd service, so that fresh comics are provided to you on a daily base.

Following comics are fetched at the moment: 
* [dilbert](https://dilbert.com/)
* [strangeplanet](https://www.nathanwpyle.art/)
* [smbc](https://www.smbc-comics.com/)
* [oglaf](https://www.oglaf.com)
* [extrafabulouscomics](https://extrafabulouscomics.com/)

### Setup

1) Create New Slack app [https://api.slack.com/apps](https://api.slack.com/apps)
2) Activate `Incoming Webhooks` for your slack app and copy the `hooks.slack.com...` url from the example curl request
3) Edit `comics_scripts/sendslack-notification` and insert the urlpart which you copied in the previous step (url looks something like this: `https://hooks.slack.com/services/XXXXXXXXXXX/XXXXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXX`)
3) Execute `install` script

### Other

The scripts also create and use `/var/tmp/lastslacksent.*` files. These files store the latest retrieved comic img md5 hash, so that each comic is sent only once to your slack channel.

#### Test service
`systemctl start comics`

#### See service terminated successfully
`systemctl status comics`

#### Reloading the deamon
In case you change the comics.{timer,service} files, make sure to reload the deamon first
`systemctl daemon-reload`
